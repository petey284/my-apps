Program Name:   NW Docx File Converter (Docx2Rtf)
Author:         Jack Lewis - NativeWinds
Creation:       2007-10-03
Updated:        2012-04-12
Version:        4.4 Freeware

Description:    A program to convert Word Doc files, Word 2007 Docx and Dotx, OpenOffice Sxw and Odt files
                to the universal rtf  format without needing MS Office 2007 or OpenOffice
                installed. Docx2Rtf can also open, view and print - pdf, html, rtf, txt files and now
                even PalmOS Database Documents (*.pdb) and Aportis (*.prc) files are supported.

Version 4.4 	Minor bug fixes.

Version 4.3	Added ability to extract plain text from standard Word doc files.

Version 4.2 	Bug fixes

Version 4.1 	New method for extracting from WordPerfect files.

 Version 3.8	Corrected problem with selecting language files in WinXP. Added command line
                option to convert files to pdf instead of rtf.

                 If you would like to convert files to pdf, you will need to add both file
                 extensions to the command line like this:

                 Docx2Rtf -f "C:\My Folder to convert" odt pdf
                 The above command line would convert all odt files found in the designated folder to pdf.

                 Docx2Rtf -f "C:\My Folder to convert" docx pdf
                 The above command line would convert all docx files found in the designated folder to pdf.

                 Docx2Rtf -f "C:\My Folder to convert" docx
                 The above command line would convert all docx files found in the designated folder to rtf
                 since the pdf extension was not specified.



 Version 3.7	Improved UniCode support.

 Version 3.6	We have (hopefully) fixed the "index out of bounds" error on XP systems that was introduced
                with our 3.5 release. If you are online when Docx2Rtf v3.6+ starts, it will now check our web
                site to see if a newer version is available for download.

 Version 3.5	Added ability to re-arrange pages using buttons at the bottom of the main screen or
		by drag and drop in the thumbnail window. You can also delete indvidual pages (if there
                is more than 1 page) using the 'Delete' button at the bottom of the main screen.

 Version 3.4	Added option to turn on/off application skinning. Docx2Rtf now allows you
		to extract text (if available) when opening pdf documents.


 Version 3.3	Docx2Rtf was updated to improve the user interface. Docx2Rtf now works fully
		on Windows Vista. 

 Version 3.2	Docx2Rtf now offers the option of viewing the raw xml data found in docx, dotx, odt and
		sxw files. We have (hopefully) corrected the problem with parsing wps files.


 Version 2.8	We have added support for underlined and italic text in OpenOffice and Office 2007 files.
		Docx2Rtf now presents a printer selection dialog before printing.

 Version 2.7    Version 2.7 of Docx2Rtf now supports multiple languages. You can select from one of the internal
                languages or load the user interface strings from a language file (*.lng) If you would like to
                create your own lng file, you can use the English.lng file included in this download as a template.
                lng files are simple text files that can be viewed inside any text editor. All language files
                must be kept in a subfolder of Docx2Rtf's location and the subfolder must be named 'Languages'.
		We believe that we have now solved the problem of the missing dtd files when converting sxw files.

 Version 2.6    Version 2.6 adds the ability to use a command line interface with Docx2Rtf.
                 --------------------------------------------------------

                 ie: Docx2Rtf -a "C:\My Document to convert.docx"
                     or
                  Docx2Rtf -a "C:\My Document to convert.odt"

                 The above command line would cause Docx2Rtf to 'automatically' convert the designated file
                 "C:\My Document to convert.docx" to "C:\My Document to convert.rtf"
                 If the file you want converted has a space in the path, you would need to enclose
                 the file name in double quotes as indicated above.

                 --------------------------------------------------------

                 You can convert an entire folder of documents at once with the following command:

                 Docx2Rtf -f "C:\My Folder to convert"

                 The above command line would convert all docx files to rtf in the designated folder.
                 If the folder you want converted has a space in the path, you would need to enclose
                 the path name in double quotes as indicated above. 

                 --------------------------------------------------------

                 If you would like to convert other file types to rtf, you will need to add the file
                 extension to the command line like this:

                 Docx2Rtf -f "C:\My Folder to convert" odt

                 The above command line would convert all odt files found in the designated folder to rtf.

                 --------------------------------------------------------

 Version 2.5    Version 2.5+ adds common images to the list of files you can view,
                including multi-page tiff files.

Email:          support@nativewindsofmontana.com
Site:           http://www.nativewindsofmontana.com
Legal issues:   Copyright (C) 2007-2010 by NativeWinds
                This software is provided 'as-is', without any express or
                implied warranty.  In no event will the author be held liable
                for any  damages arising from the use of this software.

                Permission is granted to anyone to use this software for personal
                use and redistribute it freely, subject to the following
                restrictions:

                1. The origin of this software must not be misrepresented,
                   you must not claim that you wrote the original software.

                2. This notice may not be removed or altered from any source
                   distribution.

                3. If you decide you like this software and decide to continue
                   using it, Send me an Email and tell me how you like it.
