﻿using System;
using System.Management.Automation.Runspaces;
using System.Windows.Forms;

namespace PowershellRunner
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                if (args == null || args.Length != 1)
                    throw new ApplicationException("Empty argument list");
                Runspace runspace = RunspaceFactory.CreateRunspace();
                runspace.Open();
                Pipeline pipeline = runspace.CreatePipeline();
                pipeline.Commands.AddScript(args[0]);
                pipeline.Invoke();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
