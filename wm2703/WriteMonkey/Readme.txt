﻿PLEASE REPORT BUGS OR SUGGEST FEATURES!
Discussion group: http://groups.google.com/group/writemonkey/
or write to: master@pomarancha.com
thanks

WRITEMONKEY IS A MEMBER OF ZENWARE FAMILY.
WRITEMONKEY IS FREE SOFTWARE.
PLEASE DONATE! CLICK PAY PAL ICON ON THE WEB SITE.

REQUIREMENTS
============

Windows OS (XP, Vista, Windows 7, Windows 8)
Microsoft .NET framework 4.0

If you are running Windows XP, Vista or 7, make sure that Microsoft .NET framework 4.0 is installed on your computer. If you are running Windows 8, the right framework is already there.

INSTALLATION
============

Writemonkey is a portable application so no installation is necessary. To start using it, just copy Writemonkey folder from the zip file to any folder on your disk. It is highly recommended that you keep your installation in a directory for which you have administrator rights. If you are running wm from a limited Windows account, put wm on your Desktop or any other directory for which you are sure you have complete read / write privileges. You can freely copy it to the portable disk or USB thumb drive.

Run WriteMonkey.exe file and that's it.

UPGRADE FROM PREVIOUS VERSION
=============================

You can simply rewrite all application files. Be careful when overwriting directories - if you have previously downloaded additional dictionaries, plugins, templates and/or sound schemes, be sure to keep them. The same goes for saved profiles in "profiles folder".


** IMPORTANT **  
If you are upgrading to version 2.7.0 from any previous version, be sure you also replace existing "dictionaries" folder with a new one. Old .dic files cannot be used with wm 2.7.

** IMPORTANT **  
If you are upgrading to version 2.6.0 from a version previous to 2.4.0, your old settings won't be automatically kept. In order to preserve your settings do the following: export your current profile from old version (F9), replace existing wm installation files with new ones, then import the profile from the first step.



WHAT'S NEW
==========


version 2.7.0.3 (November 8th 2014)
-----------------------------------


- changed: New spell checker and language tools. 

* Hunspell, new spelling engine which uses native Open Office / Mozilla dictionaries. Hunspell is state of the art spell checker for languages with complex word compounding and rich morphology. It is based on older MySpell standard and can also be used with original MySpell directories. Hunspell is the default spell checker for OpenOffice, Mozilla Thunderbird and Firefox, Google Chrome and the Apple MAC OS/X operating system since version 10.6.
* Support for virtually all languages (see Downloads section on more info how to get the dictionaries for languages not available on wm site).
* New spelling UI.
* You can spell check whole doc or only a selection.
* Skips internet addresses, emails, comments, tabbed paragraphs (markdown code).
* CTRL+Click word or hit SHIFT+7 to get thesaurus entries. If word is misspelled, spelling suggestions are shown.


- added: Improved plugin engine and richer API + New plugins: Clandestine files, Scratch pad, Preview ... (see Plugins section for more info)

- added / fixed: You can now do Replace All on a selection not just on the whole text.

- added: You can now show/hide white spaces from the Preferences / Screen elements menu.



version 2.6.0.3 (March 16th 2014)
---------------------------------

- changed / fixed: New version of Markdown Extra engine with many fixes. From <https://github.com/toptensoftware/MarkdownDeep>

- Fixed: Paste command caused wm to freeze in some circumstances.

- Fixed: Clicking the file name in Info bar (open Jumps window) failed when attempted second time.

- changed / fixed: Case of the lookup parameter string is now left as it is (not changed to lowercase).



version 2.6.0.2 (March 9th 2014)
--------------------------------

- added: Richer plugin API. See http://writemonkey.com/__files/plugins/WM_API_Reference_1.5.pdf for quick API reference.

- added: New plugins (Scratch pad, Easy jumps, Map jump ... See http://writemonkey.com/plugins.php for complete list.

- added: You can now jog the writing area left or right. Keyboard: use Vulcan touch CTRL+ALT+SHIFT+Left/Right or drag Infobar (empty spaces) with the mouse.

- changed: Jumps window will no longer automatically jog the writing area left or right.

- fixed: Various fixes and small changes.



version 2.5.0.7 preview 2, July 12th 2013 
-----------------------------------------

- changed (2.5.0.7): Plugins are now locked and will work only with valid writemonkey donor key.

- added (2.5.0.7): New plugin API features. No documentation yet, ask for details.

- added (2.5.0.7): Big huge thesaurus for wm plugin

- added (2.5.0.7): New white noise sound pack (City noises) - separate download

- improved (2.5.0.7): Corkboards and Clipboard picker plugins

- removed (2.5.0.7): Tweet from wm feature (donors only) was removed due to Twitter API changes and will be ported back as a wm plugin.

- fixed: Nasty crash on combination: blinds on & info bar hidden at startup.

- removed: "Inverse colors in repository" option is no longer available in this version.

- fixed: False positive "The file was saved outside wm" message when saving files to remote machine with out of the sync system clock.

- fixed / changed: Automatic word selection (when selecting with mouse) is now disabled. This allows more precise "character per character" selecting.

- changed: Updated sound library IrrKlang to version v1.4.0. for .NET 4.0

- added (EXPERIMENTAL): You can optionally show non printable whitespace characters — paragraph breaks, tabs and redundant spaces. Useful if you want to keep your text file as clean and well formated as possible. Use CTRL+8 to toggle on / off.

- added: Ability to set default wm file extension other than ".txt". Preferences / Open&Save / File format.

- added: Javascript based plugin engine with some plugins. More info below:

## Plugins engine

This will allow 3rd party extensions/plugins to add to and interact with existing core wm functionality. Since no technical documentation exists at the moment, I will just summarize some basics.

Plugins are stored in wm/plugins folder. Each sub folder represents a plugin and it needs to contain at least one *plugin_name.js* file. Wm supports javascript as it's scripting language. It utilizes Jint javascript interpreter for .NET (*http://jint.codeplex.com*) which currently implements all concepts defined in ECMAScript version 3.0. Version 5.0 is not supported at the moment.

Javascript is commonly used for handling web browser's DOM objects but in our case it can interact with:

* .NET 4.0 core functionality – Your scripts can instantiate and use all generic classes and methods. The usage is the same as in C# (.NET flagship language) with some minor differences.
* External .net libraries (dll-s) supplied with a plugin.
* Special "monkey" object that allows you to interact with different aspects of current writemonkey instance. For example:

monkey.selectedText = "this will be inserted at current caret position";

Note that you don't need to use .NET stuff at all. You can just put together simple javascript "macros".

### Included plugins

* Corkboards for wm [initialized at startup]
See instructions in hello.board. Right click blinds area.

* Big huge thesaurus for wm [initialized at startup]
Deploy: Ctrl + right click the word, or Ctrl+E,T
Ctrl+click the word inide menu = query that word

* Namely - generate random names using huge database of real names

* Smarty pants - replace straight quotes with curly ones, -- to en dash, --- to em dash etc.

* Wordwrap at 75 - hard break the text at certain position

* Quick search - find text quickly, [initialized at startup]
Deploy: CTRL+E,S
Find: start writing
Next down: Down arrow, Enter
Next up: Up arrow
First: Home
Last: End
Clear search field: Delete
Exit: Escape
Back to original location: Insert


* Block comment - add or remove "//" from the paragraphs of selected text [initialized at startup]
Deploy: CTRL+E,C (comment) / CTRL+E,U (uncomment)
If nothing is selected, paragraph under the caret will be acted upon


* Rounded monkey - show borderless window with rounded corners [initialized at startup]
Deploy / toggle: CTRL + click right side of the wm window
Set radius and opacity in js file


* Clipboard picker - keep clipboard history and allow text snippets to be accessed later [initialized at startup]
Deploy / toggle: CTRL+E,E or /clip replacement trigger


* Pomodoro - show timer and completed pomodoros with words written for each of them. Track how productive you were during the session. More about this technique at <www.pomodorotechnique.com> [initialized at startup]
Show / hide: CTRL+E,P
Click timer to start / void pomodoro
Click session label to start new session
More settings at the beginning of the plugin file


* Auto indent - On enter match the indent of the previous line, auto insert markdown bullets and increment numbers [initialized at startup]
Toggle plugin active / inactive: CTRL+E,I
SHIFT+ENTER: bypass auto indent


### Modes of deployment

Individual plugin can be initialized when wm starts or it can be deployed later using plugin menu. That depends on plugin's nature. Some plugins need to run all the time, other just do stuff when needed in a single step.

All plugin .js files with names starting with "exe_" will execute at startup. For example: exe_someplugin.js will run and someplugin.js won't.

You can deploy "one step" plugins using plugins menu - CTRL+F10. Run last used plugin with CTRL+ALT+F10. Plugin menu does show "exe" plugins (maked with green icon) but you can't re-run them. Use plugin shortcuts to deploy actions.

If implemented, running plugin can listen for keyboard (and other) events and reacts on them. Since core wm already occupies a big chunk of shortcut space, wm 2.5.0.3 adds a new feature that will allow plugins to assign shortcuts without interfering with existing combinations. To use this extended shortcut space hit CTRL+E, release, and hit another key. Plugin Quick search, for example, uses CTRL+E,S to open quick search field at the top left corner of the screen. Users can modify this shortcut by editing the plugin file.

Version 2.5.0.4 also allows replacements to not just replace text, but to execute scripts or send keyboard commands. You can for example type /go and wm will execute command predefined in replacement string. If you want to execute plugin use {full_plugin_path} for the replacement string. To execute keyboard command, use {^es} for example. That means that CTRL+E,S command will be executed. Note that {H} will send SHIFT+H and h will send just H. Visit http://msdn.microsoft.com/en-us/library/system.windows.forms.sendkeys.aspx to learn more. Plugins can register their own trigger/replacement pairs using `monkey.setReplacement(trigger, replacement)` method. Plugin Quick search for example defines trigger /s that brings up search field.

Plugins can be disabled in Preverences / Misc.

Nightly builds On Google Groups



version 2.4.0.17, December 8th 2012 
-----------------------------------

- fixed: Wm has been crashing on some machines (netbooks) with "Hide mouse pointer when writing" on.

- fixed: Problems with move paragraph command when used on last line of the text.



version 2.4.0.15, December 1st 2012 
-----------------------------------

- changed: Writemonkey is now targeted to .NET Framework 4.X and is compatible with Windows 8 without the need to install older .NET version 3.5. That also means that users with older systems who don't have .NET 4.0 installed must upgrade their machines.

- added: Single / multiple window option. You can now choose whether new instances of wm are allowed. If disabled (default) all actions will happen in the same window. Preferences / Open&Save / Open. This new functionality also makes Writemonkey usable as a default system text editor without the need for multiple windows.

- Jumps / Files — "Open in new window" command in context menu (SHIFT+ENTER) will open new wm instance with selected file. Preferences / Open&Save / Allow multiple windows must be enabled for this to work.

- added: Left and right blinds in full screen mode. Blinds mimic typing on a sheet of paper and will also lessen the screen glare when using bright backgrounds. Toggle blinds on / off with CTRL+9. Or use Preferences / Screen elements where you can choose whether to use solid or striped blinds.

- removed: Text area markers are no longer available in this version.

- added: You can now set caret position for each replacement item in Preferences / Replacements / Add, Edit. For example: Set qq as a trigger and »«~1 as a replacement. "~1" (must be at the end of the string) will tell wm that it should put the caret between the smart quotes --> »|«. That way you can start writing without moving the caret between the quotes by hand. Another example: Set a replacement for markdown link syntax - [](http://)~10 and you'll be able to write the url on the spot. Use the "Set caret position after replace" button to append ~Number automatically.

- added: Wm will check if file, while opened, has been modified by some other application / process. Then it will give you the option to reload it or not. This is the standard behavior of programs that don't put exclusive lock on files while using them.

- added: Use ALT+Up/Down to move paragraph under caret up or down.

- added: Use CTRL+0 to reset font zoom factor to 0 and text column width to 1/2 of the screen's real estate (default view).

- added: Jumps window will now show structurally indented markup headings (for Markdown, Textile and WikiCreole).

- changed: More minimal and compact representation of items in Jumps window. 

- added: Open recent Files or recent Folders in Jumps window by pressing ALT+UP or ALT+DOWN.

- added: New help menu item with descriptions of hidden features for donors & direct PayPal link.

- added: Use ALT+H to toggle syntax highlighting on / off.

- added: Sentence segment focus: use ALT+F6 to focus on a sentence under the caret. Note that the concept of sentence is not easily defined. Please test this feature for exceptions so it can be improved.

- added (experimental): Step through sentences with ALT+F7. The sentence under the caret will be selected / highlighted, then next, next, till the end of the file. You can focus on selected sentence any time by pressing F6. CTRL+F7 will step through paraghraphs.

- improved: Jumps window, bookmarks or custom view activation: jump item nearest to the current caret position is now automatically selected / highlighted.

- changed / added: Jumps filter now works differently and has more functionality: "red green blue" will match items that contain red and green and blue. Use "green -blue" to match all items that are green and not blue. You can also use "red or green or blue" to match all items that contain at least one of those tags.

- fixed: Wm has been unable to read files marked as Read-only. New behavior: Read-only file will open in Read-only mode, but because file is write protected, upon save, wm will show Save As dialog to give you the option to save changes if file has been locked by some other process after you opened it in wm.

- added: You can now set opened file to be Read-only so it can't be modified by Writemonkey or any other application until the flag is removed. This is a good way to additionally protect your valuable files. Toggle Read-only mode with CTRL+F9. In Jumps window, read-only file will have its name marked with an asterisks (*).

- added: Jumps / Folders button — in addition to existing project folders list, there is now also a list of child folders (plus link to the parent folder) of currently opened project. It helps you navigate the folder structure without the need to use "Open ..." command.

- changed: Default bookmark string from @@ to ///. This way bookmarks, if at the beginning of the paragraph, work also as comments, will be highlighted & excluded from word count and exported documents. You can still use them in the middle of paragraphs, though.

- added: Jumps window — The maximize and minimize buttons are now both enabled and have different functions. Maximize button (quasi) docks jumps window left or right and keeps it there. Minimize button undocks - jumps window stays where it is even after full screen / window toggle (old behavior).

- added: Right click on the file name in info bar brings up recent files menu. Click opens Jumps window (as before).

- changed: New application icon and new default color scheme.



version 2.3.5.0, April 26th 2012 
--------------------------------


- added: New Jumps tool - simple but versatile project management, full keyboard navigation, better filtering & other improvements. 


Keyboard shortcuts - Jumps deployment
===================================== 

CTRL + ENTER or 
ALT + LEFT / RIGHT or 
ALT + J (deprecated) or
Middle click left or right of the text column

If Jumps tool is not opened, the command will open it, otherwise it will focus existing window.

Keyboard shortcuts - Jumps window
================================= 

* CTRL + ENTER --> Select next item on the list
* UP / DOWN --> Next / previous item
* CTRL + UP / DOWN --> Jump 10 items up or down (also PGUP / PGDOWN)
* HOME, END --> Self explanatory
* ENTER, ESC --> Close Jumps. In Files view it will open selected file. The Jumps will close if you hit Enter again. ESC will always close.
* ALT + LEFT / RIGHT --> Focus main wm window
* LEFT / RIGHT --> Toggle views
* CTRL + LEFT / RIGHT --> Toggle recent folders in Files view; toggle jump masks in Custom view; no effect in Bookmarks view
* BACKSPACE --> Close Jumps and restore original caret position 
* TAB --> Focus filter box
* CTRL + 0 (zero) --> Clear filter box
* CTRL + W --> Toggle compact / full view
* CTRL + M --> Toggle mono space / proportional font
* CTRL + N --> Show hide file numbers
* CTRL + R --> Recount words in project / current view
* CTRL + C --> Copy file
* CTRL + X --> Cut file
* CTRL + V --> Paste file 
* CTRL + F --> Open Containing Folder (will close Jumps window)
* CTRL + P --> Browse projects / folders
* CTRL + S --> Toggle Classic / WM color scheme
* CTRL + I --> Insert new blank file (New.txt)
* F2 --> Rename file
* Del --> Delete file to Recycle Bin

Directory = Project
=================== 

Each directory can be a wm project. You can start new project simply by opening an existing file. Parent directory of that file will be added to recent folders / projects list. Files in that folder are project files. Wm won't deal with sub folders; you can have them but Jumps won't show them. If you want to fiddle with folders, use CTRL + P.

You can toggle projects from the recent folders list by hitting Ctrl + left/right or by clicking Folders button at the bottom. Ctrl+click will remove the item from the list. The same goes for recent files list.

By default Jumps will only show .txt, .md, and .markdown files. You can change that in Options (lower left corner). 

File order
========== 

By default files are ordered by file name. You can reorder them by drag&drop (drop file *on* another file to move it in front of it). Alternatively, you can use keyboard - Ctrl+X to cut source and Ctrl+V to paste it in front of target. You can't move files from repository to project section and vice versa. You must add or remove "draft" tag first. Custom file order is stored in _PROJECT.DAT file (project folder). (Sorry for that additional file, but there is no other viable way to store order data.)

Tags
==== 

You can mark each file with one or more tags. Tags are words (separated by space or by comma) that are marked as comment (//) and positioned at the top of the file. You can add all tags in a single line:

// tag1 tag2 tag3
or use:
// tag1
// tag2
// tag3, tag4

You can use any word as a tag. 

Special tags
============ 

There are some special tags you can use in your document header:

* Color name
+ Use valid color name (see: http://msdn.microsoft.com/en-us/library/aa358802%28v=vs.85%29.aspx) - using "red" will mark that item with red star. You can use more than one color tag - "// red, blue" or even "// red red red" if you want to use more than one star of the same color.

* Draft / Repo tag
+ Tags "draft" or "repo" will move the file to the "repository section" - file will be presented with lighter color and excluded from total word count.

* Percent tag
+ Use "10%" or "%75" ... to mark how far is this part/file from being perfect. The file progress is presented with dimmed percent bar.

* Deadline tag
+ Use "2012-02-15" or just "02-15" to set deadline date for this file. The item will turn red as the deadline whooshes by :).

* Rainbow tag
+ This is a funny one :) Use tag "rainbow" and it will mark the file with 7 stars - violet, indigo, blue, green, yellow, orange and red. Why? I don't know. Maybe you'll find a use for it :))

Tags as Colors
============== 

If you don't like to use generic color names for stars, you can assign colors to tags of your choice. Use Options in Jumps window to set appropriate pairs e.g. "todo:red, done:green, happy:gold". Using tag "todo" will mark the file with a red star.

You can even set "rewrite:red red red" and you'll get three red stars for that tag. Again, use this as it fits you best.

Filtering
========= 

Filter box is case insensitive and will filter by item file name and by it's tags. Type "red" and only files marked with red color will be shown. It will also show file named rednecks.txt. Use "red|draft" to show all draft items marked with red. Filter box suports regular expressions.

Sometimes you will want to filter by full text rather than just by tags and file name — in that case you can prepend search string with the character "&". Use "&michelle" to show all files that have "Michelle" anywhere in the document body.

Total word count
================ 

If filtering is off (empty filter box) only files that are not marked as draft will be included in the count. If filtering is on, total word count will include all shown files. 

Merging files
============= 

Options / Merge files will put project parts into a single file named _MERGED.TXT. Repository parts and repository (draft) files will be omitted. You can export with or without inline comments.


***



- added / changed: Writemonkey installation now includes RICHED20.DLL ver. 6.0 / 12.0.6606. This is to ensure that this library overrides other potentially older installed versions of rich edit control. It has many small improvements, for example: it now supports system block caret (Win7: Control Panel / Ease of Access Center / Make the computer easier to see). Also, the typewriter scrolling works better now and is out of experimental phase. 

- added: Quick mask in Custom mask view. If you prepend your filter string with a character "&" when in Custom mask view, the filter string itself will become a mask. If you use "&mike", you'll actually search for all occurrences of the string "mike". Quick jump mask is, like ordinary jump mask, regex enabled.

- added: The writing column (text area) will automatically move left or right when Jumps window is opened and will recenter when closed. Only in full screen mode.

- added: New marker in Bookmarks and Custom mask view - gray vertical stripe shows you item's relative position inside full text. You will also get the idea how far apart are individual items.

- changed / fixed: Backup directories for individual files are now named "projectName_fileName" instead of only "fileName". That way backup will properly handle files with the same name but in different project folders. Also browsing backup files in file manager is easier that way - sorting by name will nicely put together all project files.

- improved: Large markup documents will open up to 50 percent faster.

- improved: Better responsiveness when working on very large documents.

- fixed: Updated Markdowndeep (www.toptensoftware.com/markdowndeep/) library that fixes footnote issue.

- fixed: Some inconsistencies with exported lists and comment (//) lines.

- fixed, changed (version 2.3.5 preview #6): The _PROJECT.DAT file no longer holds complete file paths but only file names. The order is now preserved even if you move the project folder to some other location or use it inside DropBox folder on different machines.

- changed: Jumps "Paragraphs" item is no longer available as a stand alone option. You can reproduce this functionality by creating new Jump mask (Preferences / Jumps). Use custom mask ".+" (without brackets) to achieve that.

- fixed / improved: Lower memory consumption - improved stability and speed. Fixes: syntax highlighter memory leak -> potential stability issues; spell checker memory leak -> very high memory consumption when using multiple times; lower memory imprint for dialogs like Preferences and Jumps.

- added / improved: Better Find&Replace tool - no more nagging message boxes, Undo for Replace All command and placeholders for paragraph and tabulator ([n] and [t]). You can now for example easily convert texts with double space after paragraphs to single and vice versa ... The tool will always search trough whole text. If you want to perform Find and Replace on a block of text, use Focus command first.

- added: You can now set number of lines the text scrolls when you roll the mouse wheel (also when you use blind scrolling or keyboard scrolling). Default is 2, maximum is 5. Preferences / Misc / Scrolling.

- added: Standard Open and Save As dialogs now include Markdown filter (for files with extensions .md and .markdown). For those who prefer naming markdown files that way. When saving new "scratch" file as Markdown file, WM will always use .md extension (most commonly used).

- fixed / changed: Problems with decimal point when using math parser (ctrl+shift+R). You can now use a dot or a comma - depending on your local settings. The default is dot (en-US standard). WM is now using different math library - Parser .NET - which is also a bit faster (used to calculate page count and reading time info bar values). You can't use thousands separator (dot or comma) when doing math in WM. Parser will regard it as a decimal point and fail miserably!




version 2.3.0 November 27th 2011 
--------------------------------

- added / changed: Support for Markdown Extra standard (default). Visit http://michelf.com/projects/php-markdown/extra/ for new extra features. Core Markdown syntax is a subset of Markdown Extra - all old documents should work just fine. With one exception - rules for bold, italic and underline are slightly different than those in older versions of wm. WM now completely complies with Markdown standard! Old Markdown parser is still available as an option but deprecated. Remember also that wm supports additional markup rule - "// comments" - which is valid for all supported standards.

- fixed: A crash on some systems when using ALT-TAB to navigate away from wm and back.

- fixed: Problems on machines without working sound card.

- fixed: Stability issues with syntax highlighting.



version 2.2.0 Oktober 3rd 2011 
------------------------------


- added: Support for WikiCreole markup standard. More info: http://wikicreole.org/wiki/Home. Keyboard shortcuts for bold and italic are not yet supported in this version. No help card content either.

- added: Markup "syntax highlighting" for headings, bullets&numbers and block quotes. Paragraphs marked as headings ("#" for Markdown, "h1." for Textile or "=" for WikiCreole) will be automatically stylized based on your settings (Preferences / Colors&Fonts / [...] button). Schemes in permanent slots will hold this new info, so you can create different styles for different type of work. You may like plain centered headings for your novel and bolded&underlined for more structured pieces. Paragraphs marked as indented block quote (">" for Markdown and "bq." for Textile) will be indented by 60 pixels.  Paragraphs marked as list items (- * + 1.) will be indented properly. In order for this to work, there must be exactly one space between list mark and first letter of the text. Nest items by starting the paragraph with tabs or spaces (Markdown style), or use multiple marks (##, ** - Textile and WikiCreole). Wm highlighting helps you visually discern semantic chunks of texts (headings, block quotes, lists and comments)  and has nothing to do with printing styles. Those are defined in css export sheets and are utilized (if enabled) when exporting markup text (CTRL+SHIFT+E) or using new "copy formatted" feature (CTRL+SHIFT+F).

- added: Similar to CTRL+SHIFT+H that copies the html representation of markup text into clipboard, you can now use CTRL+SHIFT+F to copy formatted text. It will (instantly) give you the same result as the following procedure: Open Markup export options (CTRL+SHIFT+E) / Export to web browser, then in web browser window - Select All and Copy. Use this to quickly paste your markup document (or selection) to any program that supports rich text.  For example: write your elaborate and structured email using wm and Markdown syntax, hit the shortcut and paste your nicely formatted text into email client editor. Or Word ... The chosen wm template (css style sheet) is applied if enabled in Export options. If nothing is selected when using this command, whole text will be copied (no need for Select All).

- added: Drag&Drop support. You can move or copy selected text with your mouse. Hold CTRL for Copy. Remember: you can also drop text files into writing area to open them.

- improved: Faster handling  of large files (opening, toggling repository or segment focus)  when syntax highlighting for markup is enabled. Faster Jumps tool.

- fixed / improved: Filter box in Jumps is now more responsive with large files.

- fixed: Nasty desktop flickering on some XP systems when using syntax coloring.

- fixed / changed: When CTRL+TAB-ing recent files the list will show first item (not second) when employed from an unsaved document (scratch).

- fixed: Freeze when pasting over selected text from sources outside wm.

- fixed: Exception error message when exporting to ms word with previous export of the same file already opened.

- fixed: Some inconsistencies with applying meta data values (word target, partial count ...) when opening existing files.

- fixed: White noise sound won't stop when wm is closed to system tray (soft exit).

- fixed: Wm crashes when hitting CTRL+SHIFT+W and there is no white noise sound files in Sounds folder.

- fixed / improved: Annoyance when leaving focus mode or opening new file with caret restore on - the caret line always appeared at the very top of the screen. It is now conveniently shown a couple of lines down.

- fixed: Wm crashes at start on some corporate machines with FIPS (Federal Information Processing Standards) policy enforced. MD5 algorithm (used to check for text changes) has been replaced with SHA1 which is FIPS 140 compliant.

- fixed: When using Textile, paragraph marked with "#" were threated as Markdown headings (underlined).

- fixed / changed: Shortcut for "caret centering" has been causing problems on Polish systems - the users were unable to type capital letter "Ł" due to shortcut collision. Shortcuts for typewriter scrolling (CTRL+SHIFT+L) and caret centering (CTRL+SHIFT+ALT+L)  are now changed to CTRL+L and CTRL+SHIFT+L.



version 2.0.1 (SERVICE RELEASE), February 12th 2011 
---------------------------------------------------


- Fixed / added: You can optionaly disable syntax coloring. Some IME input methods (Japanese, Korean ...) won't work when coloring for // comments is on. The real fix for those languages will be (hopefully) available with the next release.

- Fixed / added: Optionally save UTF-8 files without BOM (Byte Order Mark). In some cases, text files encoded with BOM aren't compatible with other programs or text parsers. By default wm includes BOM in UTF-8 files, but you can now change that in Preferences / Open&Save.

- Fixed: Count down timer refused to beep if progress bar was hidden.

- Fixed: Some problems with syntax coloring for // comments.

- Fixed: "Hide mouse pointer while writing" did not work properly on some systems.

- Fixed: Crashing Progress dialog under certain circumstances

- Fixed: Crashing Print preview dialog when lock on top is enabled.

- Added: new experimental&hidden feature for donors - white noise player for loud environments and focused writing. Hit Ctrl+W to start playing white noise in a loop, hit Ctrl+Shift+W to toggle next sound, Hit Ctrl+W again to stop. Writemonkey will play any ogg or mp3 file found in [wmdir]/sounds directory. You must use OS sound controls to set the volume. Some additional white noise sounds are available on wm's web site.

What is white noise: Like white light, the conglomerate of all light frequencies, white noise is something we hear when there is a lot of background noise of various sources. Sound of the river, wind in the trees, sound of heavy rain ... The positive effects of those sounds on writing process can be two fold: a) they are very comforting and even have kind of hypnotic effect, b) they mask out all other sounds, especially spoken words, even when played in a very low volume. Imagine: you are writing in a café with your laptop and you don't want to be disturbed by conversations around you. Pop up the ear pieces, enable white sound feature and you are in the zone again :) 



version 2.0, January 16th 2011 
------------------------------


- changed: Version number jump. One would expect that this version of writemonkey should be called version 1.0. I think this would be somehow misleading as wm is already quite mature program and versions 1 usually aren't. 2.0 is (in my humble opinion) more appropriate. The complete numbering scheme for future releases (1.2.3.4): 1 – major release, 2 – release, 3 – service release, 4 – not needed and will be depreciated.

- added: UI language packs. Individual language packs are available for download here. Unzip xx-YY directory into writemonkey's application directory. Then change application language in Preferences / Language tab. You will need to restart wm for changes to take effect. Currently there are only few packs available; more will follow soon. Special thanks to all translators working on the project!

- added (new in 2.0 preview2): Decent "syntax coloring" engine. This version only implements it for dimming commented paragraphs, but it has much more potential. Will probably evolve in the future.

- added (new in 2.0 preview2): Inline comments solution. You can start any paragraph with two slashes "//" and the whole paragraph will be regarded as a comment. It will be dimmed on the screen (as all other "non text" elements), so you will be able to easily distinguish comments from ordinary text. Comments are excluded from word count and all other statistics. Export dialog has a new option to keep or exclude comments from exported texts. If kept, they will be presented (printed) with yellow (gray on b&w printers) background, but this could be changed in export templates. You can choose strikeout style for example … Great for printing first drafts!

Commenting as implemented in Writemonkey is a simple yet powerful (=elegant) concept known to all computer programmers. It is used for two main purposes: a) adding comments to code elements or b) for commenting out (disabling) certain parts of code with an option to easily enable them again later. Writing text is not that different process so I dare to argue that a writer should benefit too (from a and b). You can comment out meta data at the beginning of the document, mark parts that need further attention or some other kind of "note-to-myself" comments. … Or you can just comment out some parts of text – if you are over your word count limit, but still want to keep deleted parts in your original text document, for example. Or you can comment out alternative sections of your story. You may want to use them later.

- added: Reading time progress unit – It measures approximate reading time for your texts. The default value is set to 150 words per minute, but you can easily change that in Preferences / Misc tab. You can achieve very accurate results if you measure your actual reading speed and adjust the formula. Best when preparing texts for spoken media, speeches, lectures … You can show live count value in info bar or just check it in progress dialog (F12) along with other statistics.

- added: Additional text statistics. There are some new values in Progress dialog (F12) – Unique words, Average words per sentence, Reading time, Hard words (words with 3 or more syllables), Lexical density (see: http://en.wikipedia.org/wiki/Lexical_density) and Gunning fog index (see: http://en.wikipedia.org/wiki/Gunning_fog_index). You can also inspect the list of word frequencies with marked hard words. Words with less than 3 characters are not shown.

- added / improved: New info bar look&feel options. You can now optionally show inverse info bar with dimmed (background or text) color. Experiment with Inverse colors setting in Preferences / Colors&fonts.

- added / improved: New Colors&Fonts tab in Preferences dialog. Some new settings (or moved from other dialogs) and completely new permanent slots interface. You can now save and manage as many schemes as you wish. The individual slot holds values of all items in Colors&Fonts tab. This Preview version won't import your existing 4 saved schemes.

- added: Progress limit value, corresponding unit setting and partial count value are now file dependent – writemonkey will save those values as file's meta data (NTFS file system only) and restore them when the file is reopened. Note that those values are lost when you copy the file to non NTFS drive (or the file is sent via e-mail …).

- added: (new in 2.0 final) You can now lock the wm window on top of window stack. The "^" sign in status string reminds you that lock is active. Use F8 to toggle on / off. Wm will always start with "lock on top" off.

- added: (new in 2.0 final) You can now bring the typewriter metaphor to the extreme. Already present horizontal typewriter scrolling is now accompanied with optional vertical lock, so that caret will always stay fixed in the center of the screen and the text will move left and right. Like the sheet of paper in the typewriter does ;) Use CTRL+SHIFT+ALT+L to toggle on / off. It only works in full screen mode.

- Fixed / improved: Hide mouse pointer when writing. Users my occasionally experience annoying mouse cursor flickering. This is because mouse visibility is very system dependent and some background (or even internal) processes can trigger the flickering. The "hide mouse pointer when writing" workaround has been part of wm for a long time – the mouse pointer was moved to the right bottom corner of the screen, but original mouse position was not kept. It is now. Also, the pointer is now sent to middle right edge of the screen to prevent conflicts with Win7 "Hide windows" feature. This setting is now on by default for new installs; existing users should enable it in Preferences / Misc tab.

- Fixed / improved: More robust sound engine. There were quite a few reports of problems (even crashes) with typewriter sounds. This version has more robust sound engine and some dependent system files were included into installation pack in case your system installation lacks them. There are also some noticeable reaction time improvements – clicks and clacks should sound even more in sync and realistic.




version 0.9.9.0, July 23rd 2010 
-------------------------------

- added: You can now use markup export with predefined templates (CTRL+SHIFT+E). This enables you to export / print your texts with specific look and fill. You can have one template for the novels, one for poems, essays, articles ... or one for drafting, one for paper friendly version or fancy one for some other purpose ... 

Under the hood: At export, Writemonkey will first convert your markups (Markdown or Textile) to standard html elements. Added css style sheet (wm template file) will then provide visual styles for exported html elements. Created doc or html files are portable and can be opened on any modern computer. The individual template files are stored in writemonkey/templates folder and are fully portable (users can exchange them). Writemonkey uses CSS standard which is widely accepted and not hard to learn. See: http://en.wikipedia.org/wiki/CSS for details. You can modify existing bundled templates or create your own. See also http://www.w3.org/TR/css3-page/ to get the idea what is coming in css3 and what will that bring to writemonkey templating.

Templates: At the moment there are few bundled demo templates. These first templates use only basic aspects of otherwise very powerful css templating standard; much more colud be achieved with it. You are free to experiment. Suggestions are welcomed!

- added: Till now there have been two target options for markup export - Default web browser and Default doc editor. Wm 0990 brings you third - Print from preview. When selected, export will open print preview window (using Internet Explorer via .net browser component) which you can use to put your formatted document on paper. Use it also to adjust page margins, page numbers and headers/footers.

- added (EXPERIMENTAL): Typewriter scrolling (or bottom padding) is a new ergonomic feature that will allow your typing words to appear fixed at the certain vertical position of the screen. With traditional scrolling your text scrolls from top to bottom until it reaches lower edge of the screen. Eventually you will end up constantly looking down at the last line which is not most natural and neck friendly posture. Typewriter scrolling instead will keep your current writing line fixed, the text will always scroll up so your eyes never have to stray too far from one spot. At the moment the feature works well with short to medium length documents (up to 70.000 characters / cca. 50 pages of text). It will automatically turn itself off beyond that. Use segment focus to keep using it after that limit. Typewriter scrolling is off by default. Enable it in Preferences / Misc / Scrolling or toggle it with CTRL+SHIFT+L.

- added: Ability to disable visibility of standard menu bar and/or program bar (border) in windowed mode. See Preferences / Screen elements / More settings. Standard menu bar is now hidden by default; wm looks even more austere this way and all commands are available via right click context menu any way. You can combine those two settings with window transparency (CTRL+F3) and desktop picture of your choice to achieve results like this: http://writemonkey.com/img/wm_borderless.jpg. When program bar is hidden you can use the following mouse "tricks": grab right edge of the window to move the window, grab bottom right corner to resize (if info bar is visible and docked to the bottom, grab just above the info bar) ... or CTRL + right edge click to toggle border visibility.

- added: Copy and Cut commands are now paragraph aware. If nothing is selected, wm will copy/cut current paragraph (under the caret). The same goes for "move to repository" command - you can use that to write simple one line paragraph as a note and then hit CTRL+M to send it to repository for latter consideration without losing flow. 

- added / changed: Context menu (right click) now shows keyboard shortcuts for all items. This is primarily due to new default windowed look which lacks standard menu bar. It is easier that way to memorize common shortcuts.

- fixed: Problems with scroll indicator when CTRL+TABing between documents. The indicator initially showed wrong value when "restore caret after open" was enabled.

- changed: Different default positions for info bar items. It is more logical to have current time and file name on the right side and notices on the left. The result is that notices are more noticeable ;-) this way.

- changed: Info about particular preview version is now included in both splash and about boxes. No more version info in program bar for final releases. Also: about box can be called with CTRL+F1.

- added / changed: New info bar status item. When in main text "+" sign indicates that repository part of the document has some contents and that you should not forget about it. = sign indicates that typewriter scrolling is on. Plus no more square brackets for other status string items.

- added: Optionally decrease info bar font size when in windowed mode. Handy for large screens. Preferences / Screen elements / More settings.

- added: Ability to "force save" opened text using selected encoding standard. If "Preferences / Open&Save, File format / Always save with selected encoding" is checked, the text will be converted to selected encoding format. Otherwise wm will keep existing encoding (see: http://groups.google.com/group/writemonkey/msg/01526faf156eb7dc for details).

- added: New "hidden feature" available to donors: Calculate math expressions (CTRL+SHIFT+R). Transform strings like 1+1 or ((126/3,5)^2)+3*pi with a single keystroke into: 1+1 = 2 and ((126/3,5)^2)+3*pi = 1305,42

- changed: Default auto save interval is changed from 30 to 120 seconds. It should be more than enough for normal use.

- fixed / changed: Keyboard shortcut for "Copy as HTML" is changed from CTRL+ALT+C to CTRL+SHIFT+H due to keyboard conflict when trying to type character "ć" on Polish keyboards.

- fixed / changed: Support for the new twitter OAuth authentication. Old basic auth. from wm 0980 won't work after August 2010.

- fixed: Problems with help card layout when using custom system fonts size (greater than 100%).

- added: Use mouse wheel to move through tabs in preferences dialog.

- added: Writemonkey web site has it's own RSS feed with information about new releases and other important notices. Click RSS icon in the browser's address bar.



version 0.9.8.0, March 23th 2010 
--------------------------------

- added: You can now optionally use Textile markup standard (http://textile.thresholdstate.com) to export your texts. You can select your preferred markup style in Preferences / Print&Export / Markup standard. Markdown is still the default option.

- added: Complete keyboard scrolling solution. The idea is that one can scroll without using the mouse. To simulate mouse scrolling use CTRL + , (comma) to scroll up and CTRL + . (period) to scroll down. Use CTRL+SHIFT combination to scroll one page up or down (CTRL+ALT+PgDn/PgUp in previous versions). To quickly return back to the current caret position hit ALT+SPACE (ALT+C in previous versions). If you want to put caret on the current screen, hit CTRL+SPACE (the caret will be positioned in the center of current writing area).

- added / improved: Mouse "blind scrolling" feature has new functionality: you can ALT click next to the text area (left or right) and the text will scroll to the exact (vertical) spot of your click. You can also blind drag the mouse cursor while holding ALT key. This will have the same effect as dragging the standard Windows scroll thumb.

- added: Copy selection as HTML command (CTRL+ALT+C). Markup will be translated into well formed HTML and stored in Windows clipboard. If nothing is selected, the whole text will be copied. The copied HTML won't include "Head" items as is the case when you use Export option.

- added: Many users run wm on their laptops or netbooks in full screen so there is no way to see how much battery juice is left. This version brings you the ability to show battery status in info bar. The status mark ±X% will only show when the percent of juice remaining is lower than value stored in Preferences / Screen elements / More settings. The status mark won't show at all when computer is on power line. The feature is disabled by default.

- added: When "soft exit" is enabled (Preferences / Open & Save / Exit) you can force "hard exit" by using CTRL+SHIFT+Q or holding SHIFT when closing the program with mouse click. If soft exit is disabled, the shift key has no effect.

- added: Adjust the Opacity of wm's workspace. Use CTRL+F3 to toggle value from 100 to 30 percent.

- fixed: Scrolling with "mouse wheel" using touch pad on some laptop computers. The scrolling should now be smooth and fast enough. Also, some advanced touch pad functions like "edge continuous scrolling" should now work just fine.

- fixed (partially): Mouse pointer flickering when writing. The flickering occurs every time the visual progress bar is updated (it's size is changed). It is a very strange bug and I do not know why it happens nor I am able to fix it. Nevertheless, I managed to eliminate flickering when visual progress bar is not enabled or when auto hide info bar function is on.

- fixed: If you quit WM with unsaved "SCRATCH" document, it asks you to save the file. If you select "Yes", the "Save As" dialog is presented. In all older versions, including 0971, wm will quit if you choose Cancel at that point. 0980 will just close the Save As dialog and keep the itself alive. This is expected behavior for all Windows applications.

- fixed: You can now use space as a thousands separator in info bar progress string (thanks to Jan).

- added: New notice that will tell you if repository has contents when you open an existing file. 

- added / changed: New optional tag next to the scroll marker that shows you the scroll position in percent. It only shows when scroll marker is visible i.e. when scrolling, jumping, moving with cursor ... Percent tag is off by default. Enable it in Preferences / Screen elements / More settings.

- changed: Different directory structure in installation zip file. You can now "install" wm with single mouse move.

- changed / added: Markup export feature is now selection aware. You can export only a selected portion of the document. If nothing is selected the whole document will be exported.

- changed: There are few hidden / not documented features (easter eggs if you want) in WM. One has just been added for this release. Hidden features are not important features and they are certainly not part of core wm feature array. They are more like nice hidden cherries. From now on all hidden features are available only to donors i.e. owners of wm.donated files with writer's name tags. And what are those secret features? You will know if you read writemonkey Google group or Twitter posts ;-)




version 0.9.7.1, January 15th 2010 (service release)
----------------------------------------------------

- fixed / changed: You can not use space as a thousands separator any more due to problems with storing single space in settings xml file.

- fixed: WM died sadly when attempting to open or save file in root directory.

- fixed: Problems with existing text files using ANSI encoding. WM now handles all encodings correctly. How it works: If you open new document from scratch, wm will use the default encoding (Preferences / Open&Save / File format). The UTF-8 standard is recommended for majority of users. If you open an existing (not created in wm) txt file, wm will use the encoding of this file and will also save the file with it. That is: wm won't force it's own default encoding to the existing files. 

There are cases when wm (and any other editor for that matter) can't reliably detect the used standard:

Basically, the beginning of the file may or may not contain a Byte Order Mark (BOM). If it does, its encoding is immediately clear. If it doesn't, the file could be encoded using UTF-8 or ANSI and there is no full proof way to detect which one is used. We need to make an educated guess. In that case wm will do the following:  first it will assume that the file is UTF-8. If file is actually in ANSI format and there are no non English characters in it, the file will be converted to UTF-8 format and nothing will be lost or changed. If file is in ANSI format and there are foreign, non English characters in it, then wm will keep that file in ANSI format. And again, nothing will be changed or corrupted.

If you want to change the encoding of an existing file to the default wm encoding, do the following: Open existing file (ANSI for example), copy all, close that file and start new from scratch, paste the contents in to the new doc and save. You now have the file with wm's default encoding.

- changed: Single letter status marks



version 0.9.7.0, January 8th 2010
---------------------------------

- added: New info bar replaces old progress bar and adds some new functionality. Info bar can be docked to the bottom or top and can contain 5 info items (2 left, 2 right and 1 in the middle). The items could be positioned freely or disabled for that matter. The good old visual progress bar is still there. Also the ability to auto hide info bar when writing. ALT-P toggles the visibility of info bar. 

Info bar items:

	* Configurable date/time. Go to Preferences / Screen elements / More settings to set the format.

	* File name. You can choose to show only the file name or file name with the path. Only the parent directory and drive letter is shown i.e. "c:\..\work\some.txt". Click on the info bar item opens "Open file" dialog.

	* Progress. The progress string can contain current word count, partial count and / or sprint timer. The progress string is configurable through Progress dialog (F12 or click on the info bar item).

	* Notices. Notices are only shown for a couple of seconds. They inform you of stuff going on in Writemonkey. It is nice to have a visual confirmation that the file was actually saved when you hit CTRL+S ...

	* Status. Status marks are permanent reminders of your whereabouts. You will always know if you are in focus mode, flow mode or in repository ...

- changed: Settings and Progress dialogs have been changed. There is a new Screen elements tab in Settings dialog. Progress dialog shows stats and provide settings for info bar progress string.

- changed: All non text screen elements (new info bar, visual progress bar, text area marks and scroll indicator are now members of the same family. You can dim all of them with one slider. 

- added / changed: There is an option now to allow you to trigger replacement only after the space is pressed (instead of auto fire). No more "collision" problems - the triggers like /t /tr /tri can now coexist peacefully with each other. This is enabled by default. It is also the recommended option - the "space" method is faster which could be of importance with very long documents.

- added: There is a new button "Make replacement" in Insert symbol dialog. You can quickly add new replacement trigger for the selected symbol. 

- added: CTRL+SHIFT+C keyboard command copies whole text to the clipboard. Same as CTRL+A CTRL+C but more handy.

- added: Improved sprint timer. You can toggle sprint timer (paused/running) with ALT+F11. No need to open Progress dialog. Also: when time is up, wm will inform you about the fact with a notice and 3 short beeps.

- added: You can include "percent done" in the progress string for the info bar. Progress dialog / Progress count / Show percent. For example, the count string will look like: "Words: 100 / 10%" if your goal is 1000 words. Show percent, if enabled, will also show in partial count.

- added: You can choose weather to show count unit label (Words:, Characters: ...) in progress string. Progress dialog / Progress count / Show unit.

- added: Thousands separator can be customized (dot, comma or even space). Preferences /  screen elements / More settings.

- fixed: Lookup url string is stripped of unwanted punctuations (.,;:?!).

- fixed: Some dialog colors has been changed from "absolute" to "relative" system colors. WM now looks good even with more exotic windows themes (color schemes).



version 0.9.6.0, November 19th 2009
-----------------------------------

- added: Segment focus feature. Ability to show only a segment of the current text and hide the rest. Good for longer & structured texts - Will help you to focus on the part you are working on. No long scrolls ... You can focus on different things using  different shortcuts:

F6: Hide all focus: if nothing is selected whole text will vanish and you will start inserting text at the caret position.

F6: Block focus: if you select a block o text and hit F6, only selected text will stay on the screen.

CTRL+F6: Paragraph focus: Hold CTRL when hitting F6 (or selecting "Segment focus" from menu) and the current paragraph will stay in the focus.

SHIFT+F6: Heading section focus: Hold SHIFT and you can work on the current heading section (#, ## / ==, -- markup)

SHIFT+CTRL+F6: Bookmark segment focus: You can work on the text between bookmarks (@@ or user defined string)

You can exit segment focus by hitting F6 again. Existing documents will always open with segment focus off, even when last saved focused. There is a subtle visual reminder when segment focus is activated - the corner marks (if visible) are presented with dotted line. Exit segment focus and corner marks become solid again.

Alternative usage: You can use segment focus instead of partial count - start writing with "Hide all focus" and word count will show only current session. If you have a large repository text and you need to reference a part of it often, focus on that part and you can easily access it later. If you want to spell check only part of the doc. you can do it on a segment focus.

- added: Complete Markdown support. See: http://daringfireball.net/projects/markdown/syntax The only exception is emphasis syntax (*bold*, _italic_, __underline__). 

- changed: The application is now targeted to .NET framework ver. 3.5. The users with .NET 2.0 should upgrade to 3.5. This is due to some development features I intend to use and are not available in 2.0. One of the advantages is also much smaller memory usage.

- changed: New shape and new position for scroll marker. The new position (near to the text) is optional - the old one is still the default one (Preferences / Misc / Scrolling / Next to the text).

- added: Progress bar now supports pages. You can set your own page count formula in Preferences / Progress / Page count. Then hit F12 and select "pages" from "Use unit" drop down. Your live count will now give you the estimate of pages written in 0.0 format.

- added: You can optionally show live numeric count with thousands separator. Off by default. (Preferences / Progress / Use thousands separator)



version 0.9.5.0, August 25th 2009
---------------------------------

- added: Lookups - new tool for easy querying customized online resources. Just point to the word and hit ALT+1-9 and the default web browser with the definition will pop up. You can manage your resources in Preferences / Lookups. Select a block of text to lookup multiple words. The feature works best when using two monitors. Put wm on second monitor, web browser on first (both in full screen). When writing, hit lookup shortcut and resource will show on the first monitor while wm will stay full screen on second. You can even write on while waiting for definitions to load ... By default, resource will open in default web browser, but you can use any other application that takes url as an opening parameter. (Preferences / Lookups / Use other than default browser).

- added: Quick jump feature. Lock scroll position in current doc. and than quickly jump to it. Hit CTR+F4 to lock. Press&hold F4 to jump, release to jump back.

- improved: More customizable page count option. The new count is based on custom formula (Preferences / Progress / Page cont formula). 

- improved: Better typing sounds engine - different key sounds for more authentic experience. New default sound schemes (Old typewriter and Click keyboard). 

- added: Shortcut (ALT+F11) to toggle progress bar type (numeric / visual).

- added: Markup export now recognizes URLs and e-mail addresses and makes them clickable in exported text.

- added: Change help card pages with keyboard (Left key, Right key)

- fixed: Visibility problems with opened dialogs (like Jumps or Find&Replace dialog) when quiting to system tray (soft exit).

- fixed: Problems on some systems when trying to load saved profiles.

- fixed: Inconsistent behavior when selecting Cancel in "Do you want to save changes?" dialog.

- fixed: No typing sound for some keys (?;:_<>)

-changed: Shortcut keys (ALT+B, ALT+N) for next and previous bookmarks are reverted. It is more logical this way.

- added: Some more monkey wisdom for the splash screen.




version 0.9.4.0, May 30th 2009
------------------------------

- added: Ability to export or import user profiles (Preferences / Manage profiles button or F9 shortcut). You can export all current setting into a file and then restore it if needed. For example, you can keep different profiles for different tasks. You can also restore to default "factory" settings. The profiles are stored in [application_directory]/profiles.

- added: Default portable settings profile. Is you save profile with the name "portable" (i.e. if valid file "portable.config" exists in profiles folder) and wm is installed on a USB thumb drive, the settings from portable config will automatically become default settings on the host machine. This feature enables you to prepare yourself a portable Writemonkey installation with personalized settings.

- added: Partial count. When using numeric progress bar, hit ALT+F12 (or use Progress dialog) to show/reset partial word count. Usage example: The document already contains some text and I want to write another 1000 words from now on. So I'll reset partial count and monitor my partial progress. This feature is not available with visual progress bar.

- added: Restore caret position when opening files. If this is enabled, the existing files will open with last caret position (at the end of the document for example). This feature only works with NTFS file system (default on Win XP, Vista and Win7). Future releases will employ other file based preferences.

- added: File name is suggested in Save As dialog when saving unsaved doc. The suggestion is based on document text and prefix wm_.

- added: Undocumented feature - Flow mode. Use your Vulcan touch and hit CTRL+SHIFT+ALT+F. Backspace and delete buttons will be disabled. No cut, copy and paste. For first drafts and other creative seances. Just let it go ... You can exit the flow mode only by restarting wm.

- fixed: Maximize window button now performs wm's full screen maximize instead of Windows default maximize behavior which was not necessary and also somewhat buggy. You can still double click the program bar to toggle between maximized and windowed state.

- changed: PgUp and PgDown keyboard keys are now also moving the caret (this is Windows default behavior). If you want to use them "scroll like" with static caret - use CTRL+ALT combination. PgUp and PgDown keys can also be used with other control keys combinations. Experiment.



version 0.9.3.0 [beta], March 27th 2009
---------------------------------------

- added: New accessibility feature - you can now optionally "Quit" Writemonkey to the system tray (soft close). In this case wm is actually not closed but hidden. Access it again by left clicking the system tray icon. Right click system tray icon and select "Quit Writemonkey" to close the program permanently (hard close) or select item from the recently opened files list. This feature is disabled by default - see: Preferences / Open&Save / Quit to system tray.

- added: A personalized tag "WRITER: WRITERS NAME" in splash screen for those who have donated to Writemonkey's PayPal account. The default tag for those who haven't is "WRITER: ANONYMOUS". Just a little push ;-) WM is still *completely free*. There is no time limitation and all features are fully functional. All donators will receive personalized file "wm.donated" which must be copied in to the wm application directory.

- fixed: correct tab order and default accept key (enter) for some controls like Find&Replace ... You can now use these tools without the mouse.

- fixed: Nasty crash on start up if system has no valid sound device (even if sounds were disabled). From now on wm runs fine without installed sound device. The sound controls in properties dialog are disabled.

- fixed: Couple of minor bugs and inconveniences.

- added: New batch of monkey wisdoms.



version 0.9.2.0 [beta], March 5th 2009
--------------------------------------

- added: Markup export option. You can use wm markup to structure your text and/or apply basic formatings and then export nicely formated text to MS Word or default web browser. Hit F1 (help card) to see basic markup rules. WM markup is based on Markdown standard, originally created by John Gruber and Aaron Swartz (daringfireball.net).

- added: New included jump mask - Headings. Use it to see the markup structure of your text.

- added: shortcut for __underline__ marks: CTRL+U.

- added: All markup shortcuts are now toggable. For example: Hit CTRL+B to add bold marks, select and hit again to remove them.

- added: Option to use monospace font in jumps dialog. Jumps / Options.

- fixed: wm jumped out of focus when closing preferences dialog.

- changed: Different default values for some settings. It won't have any effect if upgrading from previous version.



version 0.9.1.0 [beta], Feruary 9th 2009
----------------------------------------

- added / changed: You can now jump through bookmarks into both directions (ALT+B: search down, ALT+N: search up). The "beep beep" sound is played when there is no more matches in chosen direction. 

- added: Scroll position marker is visible when browsing jumps dialog items or jumping through bookmarks using shortcuts.

- added: Optional item numbers in Jumps dialog for easier navigation.

- added: Ability to set custom numeric progress bar font.

- changed: Numeric progress bar format is now even more minimalistic.

-added: Windowed position and size are kept between sessions.

- changed: Different default values for some settings. It won't have any effect if upgrading from previous version.



version 0.9.0.2 [beta], January 26th 2009
-----------------------------------------

- fixed: User settings were not kept when upgrading from previous version.


version 0.9.0.1 [beta], January 25th 2009
-----------------------------------------

- fixed: Compatibility issues with irrKlang sound library on 64bit systems. 

This release does not include any new features. No need to update if running on 32bit system.


version 0.9.0.0 [beta], January 13th 2009
-----------------------------------------

- added: Finally ability to set paragraph indentation (normal or hanging). See Preferences / Layout.

- added: Jump masks. You can use predefined or create custom regular expression mask. Predefined masks include: headlines, comments, numbering, quotes, tags and links. You can provide your own masks that match your text formatting style, but some knowledge of how regular expressions work is required. New jump masks will be available on wm web site as users provide them. See Preferences / Jumps and Jumps dialog to play around. 

- added: Ability to set typing sounds volume. The volume is independent of the main win volume control, so you can adjust key sounds loudness to the music you are listening in the background ...

- changed: Version 0.9 brings you new default color scheme "Shades of green". Easy on your eyes.

- fixed: nasty typing sounds lag on some systems.

- fixed: Overhead red progress bar line was invisible in some cases.


version 0.8.9.0 [beta], December 13th 2008
------------------------------------------

- added: Numeric progress bar. You can now choose the type of progress bar - visual or numeric. Numeric progress count has been one of the most requested features in WM. And here it is ...
(tip: Select portion of your text to see count for selected text only. This option is available only with numeric type.)

- changed: Due to compatibility issues progress bar can not be positioned to the left side of the screen any more. Only to the top or bottom.

- added: Support for *bold* and _italic_ markings. This is as far as WM will go with basic formatting options. Many users requested them, but WM is and will stay a *text* editor for writers. No real rich text, sorry. You can use bold and italic marks as you write (standard CTRL+B, CTRL+I commands) and then apply AutoFormat / AutoCorrect command in Microsoft Word, OpenOffice or other programs with that feature to replace them with real formattings.

- fixed / changed: Esc closes the Preferences dialog, preferences dialog remembers last opened tab, find and replace dialog remembers searching parameters ... some other fixes and refinements.


version 0.8.8.0 [beta], November 13th 2008
------------------------------------------

- added: Sprint writing mode. You can set timer and put the mark on the progress bar. That gives you the info on how much you have written and is there enough time left to achieve your goal. Hit F12 (Progress dialog) for settings.

- added: Progress bar position setting (left - default, top or bottom of the screen). 

- added: Page break string. Use *** (or set different string) to force page break. This only applies for printing.

- added: You can monitor your progress based on number of paragraphs written.

- fixed: Word count is now accurate.

- added: Optionally hide mouse pointer to the bottom right corner of the screen when writing. Due to flickering problems on some systems. This setting is on by default (Preferences / Misc).

- added: Help for date/time formatting.

- changed: Different default values for number of settings. If you are upgrading from previous version it won't have any effect.

- changed: Progress dialog is now toggable. Hit F12 to show and F12 again to hide.

- fixed/changed: Some other fixes and refinements.


version 0.8.7.0 [beta], October 5th 2008
----------------------------------------

- added: Auto replacements - a tool that can be used in at least two different ways: you can use it as a special character replacement or as a shorthand to insert predefined strings of text such as signatures, addresses, date-time stamps etc. Go to Preferences / Replacements to add your own trigger/replacement pairs. 

- changed: Complete rewrite of Jumps tool. You can now browse by bookmarks and by all paragraphs using wild card (* and ? characters) enabled filtering. The Headlines option was removed due to lack of consistency. 
Tricks: 
Right click on item -> jump and close
CTRL + BACKSPACE -> close and return to start caret position
Hold CTRL -> jump on mouse hover

- added: Ability to set date-time format which can be used with replacements tool. For example use trigger "/dt" to insert current time stamp.

- added: Insert symbol dialog with collection of recently used items.

- added: Paragraph count in Stats dialog.

- added: Ability to set standard page parameters in Stats dialog.

- added: New delete/backspace sound for both bundled typing sound schemes.

- added: Insert bookmark menu item. Many users didn't know how to use bookmark string ;-)

- fixed: Current selection was lost when scrolling.

- changed, fixed: Some other minor changes and fixes.


version 0.8.6.0 [beta], August 17th 2008
----------------------------------------

- added: Scroll position indicator - a little dot at the right edge of the screen is visible only when needed i.e. when scrolling, or navigating by pressing PgDn/PgUp or Up/Down arrow keys.

- added: When blind scrolling with right mouse click, hit Control key to jump page up or page down. Release to scroll one line again.

- added: Stats (F12) now include sentence count and average characters per sentence. The algorithm is not perfect - the sentence is any block of text, that is ended by . ! ? or ;. 

- added: You can set progress bar to be visible only when idle for certain number of seconds. Check Preferences / Misc / Progress bar.

- changed: PageUp and PageDown keys are scrolling text and not moving the caret. You can return to starting point by hitting ALT+C.

- added: Some additional monkey wisdoms.


version 0.8.5.5 [beta], August 8th 2008
---------------------------------------

- added: Finally decent Find and Replace tool. You can search using wild characters or even regular expressions.

- added: The ability to set number of spaces to use instead of tabs. Check Preferences/misc.

- added/changed: CTRL + Mouse wheel: text area width and text zoom factor are changed proportionally; ALT + Mouse wheel: change text area width; SHIFT + Mouse wheel: change text zoom factor only. 

- changed: Screen zoom factor is maintained between sessions. When you come back, the screen will look exactly as you leave it.

- fixed: Inconsistencies with auto save when toggling recent files with CTRL+TAB.

- fixed/added: The caret *and scroll* position are maintained when switching between main and repository text.

- fixed: Blind mouse scrolling is now consistent with top and bottom margins in full screen mode.

- fixed: Some potential problems with typing sounds buffering. Especially on Vista machines.

- fixed: Some other minor fixes and changes.



version 0.8.5.0 [beta], July 20th 2008
--------------------------------------

- added: Ability to set top and bottom margins of writing area in full screen mode. Handy when using WriteMonkey on large screens. You can, for example, increase bottom margin to avoid looking at the very bottom of the screen. Helpful for your eye and neck comfort.

- added/changed: Lock background or text color when using Try luck button in Preferences / Colors&Fonts. Hold Control key to change only text color, hold Alt to change only background color.

- fixed: If Windows shutdown (or log off) is forced, Writemonkey saves the current document before system closes it. In case of blank unsaved document (Scratch.txt), the file WriteMonkeyRecovery.txt is saved to the user desktop.

- changed / fixed: New splash screen and some other minor fixes and changes.


version 0.8.4.2 [beta], July 16th 2008
--------------------------------------

- fixed: Potential problems with system fonts when running on Vista.


version 0.8.4.1 [beta], July 8th 2008
-------------------------------------

- fixed: Some potential problems with typing sounds when upgrading from older versions.

- added: Quick peep function to briefly hide Writemonkey. Useful to see what's happening behind without unnecessary fuss. Press F3 to hide, release to bring back. Suggested by Mike.

- fixed: Some other minor fixes and changes (new about box).

version 0.8.4.0 [beta], July 4th 2008
-------------------------------------

- changed: Typing sounds were completely redone and are now much more realistic. There are three different sounds - key sound, space sound and enter/new paragraph sound. You can use different sound schemes - some are available on Writemonkey web site, but you can create your own. Just add appropriate wave files in "sounds" directory. If you are happy with your scheme, please send the files to me, so I can make them available for other users to download. Thanks.

- added: Text area markers for better onscreen orientation. They are optional. Turn them on / off (or customize them) in Preferences / Layout tab.

version 0.8.3.5 [beta], June 15th 2008
--------------------------------------

- added: Drag and drop file into text area opens that file.

- added: CTRL + TAB: toggle recent files (Windows ALT + TAB style).

- added: Setting for maximum number of recent files items (Preferences / Open&Save tab).

- changed: Shortcut for toggling bookmarks is now ALT + B (instead of CTRL + TAB).


version 0.8.3.1 [beta], May 27th 2008
-------------------------------------

- fixed: Netspell library was fixed and rebuild and now supports languages like Slovenian, Croatian, Russian, Greek ... Some new dictionaries are available for download. Request more!


version 0.8.3.0 [beta], May 26th 2008
-------------------------------------

- added: Advanced backup/history system. Writemonkey will automatically create multiple, time tagged backup files of your work. Each document you are working on has it's own folder in WriteMonkeyBackups directory (on your Desktop by default). You can set maximum number of backups for both - manual and automatic save. There is also "LastSave_filename.txt" file which always contains last saved version. The backup files are out of your way (no annoying .bak files in working directory!), but you can browse them easily if necessary. Cure for accidental deletes and save based disasters. The backups are off by default - use Preferences / Open&Save tab to enable them.

- changed: Properties dialog was rearranged.

version 0.8.2.0 [beta], May 7th 2008
---------------------------------------

- added: Small and simple icons for both - main and context menu. Hope you like them.


version 0.8.1.0 [beta], April 30th 2008
---------------------------------------

- changed: Printing was completely rewritten. You can now preview before printing and set page properties (i.e. paper size, orientation and margins). There is a new tab in Preferences dialog, where you can choose to print page numbers (top or bottom) and save preferred printing style. You can print with current screen settings or use print specific formatings - font, line spacing, paragraph spacing and text alignment. For example - you can always print with basic Courier type and double line spacing, but use more appropriate font and spacings when editing text on screen. Printing is "selection aware" i.e. you can print only currently selected text.

- added: 100+ new monkey wisdoms for your amusement.

version 0.8.0.0 [beta], April 11th 2008
---------------------------------------

- added: Integrated spell checking for many languages (using powerful Netspell engine). The program file comes with English dictionary, other are available for download in separate zip file. You can spell check whole document or just current selection. Just hit F7. See also Preferences / General settings for some other options.

- added: Writemonkey is now compatible with great Firefox extension It's All Text (https://addons.mozilla.org/en-US/firefox/addon/4125). Use it when writing text for your blogs, for web based e-mail ... It works with any textarea on any web page. To use this extension set Writemonkey default character encoding to UTF-8 and check out Always save on exit (Preferences / General settings).

- added: Two shortcuts for changing the case of selected text: ALT+up - from lower to upper, ALT+down - vice versa.

- changed: The default character encoding is now set to UTF-8. For greater compatibility.

- changed: "Monkey says" will never pop up when opening existing file using Open with Writemonkey in Windows context menu.

- changed: Preferences dialog was rearranged. Other settings is now named General settings. There is also a new tab - Spelling.

- fixed: some other performance issues and bug fixes

version 0.7.9.7 [beta], March 26th 2008
---------------------------------------

- added: Quick find in Jumps dialog.

- fixed: Some inconsistencies in Jumps dialog.

- changed: In Jups dialog: CTRL + Backspace = return back to starting point and close (instead of just Backspace)

version 0.7.9.5 [beta], March 1st 2008
--------------------------------------
- added: Left click "blind scrolling" is improved: if you click at the very top or very bottom of the screen / window, the text is scrolled to the top / bottom. Same as "Home" and "End" buttons.

- added: Basic support for multiple monitors. You can use extended Windows desktop feature (in Windows display properties) and run Writemonkey in full screen on any monitor connected to your computer. This is also a fix - there were some problems when using monitors with different resolutions. Thanks to Jeff.

- fixed: The user settings are now preserved when upgrading to new version of Writemonkey.

- added: Ability to restore all settings to their default values. See: preferences / restore defaults.

version 0.7.9.1 [beta], February 18th 2008
------------------------------------------
- fixed: The text in repository is now safe even when using Windows new lines (default in preferences / other settings). Thanks to Mike and his quick insight.

version 0.7.9.0 [beta], February 17th 2008
------------------------------------------

- added: New Layout tab in preferences dialog. Ability to set text column width (moved from Other settings), line spacing, paragraph spacing and text alignment (left, right, centered or justified). Beware that these settings do not apply when using built in printing. Not yet anyway.

- changed: Scroll buttons were removed, because there is no need for them. You can still scroll by clicking next to the text area. Upper half will scroll up, bottom half will scroll down. You can disable this feature in preferences / other settings.

- added: Jump back to caret shortcut (ALT+C). Useful when scrolling through text and want to quickly return to the current writing position.

- added: Option to save files with Unix line breaks. \n instead of \r\n which is Windows standard. Unix line breaks are recognized by majority of Windows programs, but not by Notepad. Good for greater portability to other platforms.

- added: Now you can choose weather to use regular tabs (\t) or plain spaces instead of them.

- fixed: Text modification state is now preserved when changing preferences or colors. Ask to save dialog is shown only when needed.

- fixed: Open with Writemonkey check box state in preferences dialog is now correct even when upgrading to higher version of WM.

- changed / fixed: URL addresses are now clickable in repository and main text. This is due to reported framework bug - you can not change the DetectUrls at runtime. See: http://support.microsoft.com/kb/814310

-------

- BUG!!!: in some circumstances the repository text looses line breaks. It has something to do with Windows line breaks \r\n. It seams that this is a framework bug, but I'm not sure if that really is the case. There is a dirty workaround: the bug will disappear if you use Unix line breaks. I'll fix that in the future. PLEASE REPORT YOUR INSIGHTS.



version 0.7.8.1 [beta], February 10th 2008
------------------------------------------

- fixed: backgrounds of all icons are now transparent as they should be.

- added: shortcut to minimize Writemonkey to system tray (F2). Handy when working in full screen mode.


version 0.7.8.0 [beta], February 1st 2008
-----------------------------------------

- added: Jumps - an innovative tool for navigating documents. You can jump using:

Bookmarks (the default bookmark string is ">>")

Headlines (paragraphs in capital letters not longer than 50 characters)

Paragraphs (paragraphs longer than 50 characters)

Click = jump to specific location in text
Right click = jump and close
Backspace = return back to starting point and close

There are some other controls and settings - Recent files list and Tools. Fell free to explore.

The jump window can be toggled by pressing ALT-J, or by middle clicking left or right of the text area. The window can be repositioned or resized - new settings will be saved.

Your feedback about this new feature would be appreciated!

- added: Vista icon (256x256 resizable png) and some other sizes for large screens (96x96, 72x72 bmps).

- fixed: some minor changes and fixes.


version 0.7.7.0 [beta], January 18th 2008
-----------------------------------------

- added: Simple but handy bookmark system. Insert bookmark string anywhere in your text (the default is ">>", but you can personalize this in Preferences dialog) and then toggle between bookmarks with CTRL+TAB shortcut. When the last bookmark is reached, the beep sound is played and the search continues at the top of the document. This feature will be expanded and refined in future versions. Please send feedback.

- added: Handy Help card with shortcut list. Just hit F1.

- added: New more pleasant and authentic default typewriter sound (under number 1). There are still four other to choose from.

- added: Additional shortcut for toggling repository / main text: ALT + R.

- added to the web page: Microsoft Consolas font - for best experience Monkey recommends this new free font. Visit Downloads to get it.

- fixed: Some spelling errors in Preferences dialog box. Thanks to Mats Bleikelia.

- fixed: Some other minor changes and fixes.


version 0.7.6.1 [beta], January 4th 2008
----------------------------------------

- added: Some control over text encoding. You can set the default text encoding scheme in Preferences / Other settings. All new files (Scratch) will use it. You can choose between Unicode, Unicode big endian, ANSI and UTF-8 code tables. The preset Unicode should work Ok. for the majority of us, so if everything is Ok. and you don't know what to do with this setting - just don't bother.

Encoding of old (opened) files is preserved - the opened ANSI text will be saved as ANSI, UTF-8 as UTF-8 ...

- fixed: Because of new encoding features, text files are not always saved as ANSI text. This should prevent potential problems when saving files with international characters.

- fixed: Some other minor fixes.


version 0.7.5.0 [beta], December 18th 2007
------------------------------------------

- added: New repository mode for each document. Now your text document can be divided into two parts. The main text part is where your writings reside. The second part is repository for storing notes, unfinished parts, data clippings or URL's which are automatically recognized as active links and are clickable. 

You can easily toggle between two modes and move parts of text back and forth. To avoid confusion the "Inverse colors in repository mode" can be set in Preferences / Other settings (already on by default).

If there is content in repository part, the text file is saved in format: 

"main text***END OF FILE DIVIDER***repository text".

You can see that if you open file in Notepad or some other text editor. This feature was initially proposed by Nikolaj Pečenko. 

- added: New icon set matching the current default color scheme (yellow on green). ICO file now includes multiple size icons (16x16, 32x32, 48x48).


version 0.7.1.0 [beta], November 16th 2007
------------------------------------------

- added: New keyboard sound (number 5) emulating good old "click" computer keyboard. Requested by Miha.

- added: You can optionaly set default printing font. If this is enabled, the printout will allways use chosen font instead of onscreen one. If disabled the current onscreen font will be used. Go to Preferences / Other settings / Printing. Requested by Karina.


version 0.7.0.0 [beta], October 20th 2007
-----------------------------------------

- added: Killer feature for journalists! Now you can set the maximum number of characters or words and display the progress bar of your work. When limit is reached, the red overhead portion of the bar appears. This is appropriate when writing stuff which needs to be exactly of certain length, and you want to intuitively monitor your progress. The feature was proposed by Monitor magazine columnist Nikolaj Pečenko and welcomed by many of our journalist friends.

- added: "New from clipboard" command in File menu was added to quickly get your text into WriteMonkey. The new document is created with text from clipboard already in place. Of course copy and paste works well too.

-added: Shortcut for toggling progress bar on and off. Use ALT + P.

- added to the web page: WriteMonkey is still free, and always will be, but from now on you can donate through our Paypal account. It is not all about money - but if you send us some, we'll see that our efforts pay off and continue to develop this program.

- fixed: Shortcuts keys for changing screen zoom factor and text width are altered due to some conflicts with other commands. Instead of ALT + arrow keys use CTRL + ALT + arrow keys.

This release also implements some other minor features, bug fixes and cosmetic changes.


version 0.6.2.1 [beta], August 3rd 2007
---------------------------------------

- fixed: Potential problems when adding "Open with WriteMonkey" command in Windows context menu i.e. adding right values to the registry.


version 0.6.2.0 [beta], July 31st 2007
--------------------------------------

- added: Optional integration with Windows context menu. Right click on any file (even files with no extension) will show "Open with WriteMonkey" command. 


version 0.6.1.0 [beta], July 26th 2007
--------------------------------------

- added: Three additional typewriter sounds to choose from.

- added: Shortcut for toggling typewriter sound on and off (ALT + S).

- added: Shortcut for generating random color scheme. The same as Try luck button in preferences dialog (ALT + L).

- added: 150 additional "Monkey says" wisdom quotes.

- added: List of all keyboard shortcuts in online help section.


version 0.6.0.2 [beta], June 16th 2007
--------------------------------------

- added: Ability to set text width and text zoom factor using shortcuts (ALT + direction keys). 

- fixed: Some problems with typewriter sound when applying Preferences dialog more then once per session. 


version 0.6.0.1 [beta], May 15th 2007
-------------------------------------

- added: "Try luck" button in Preferences / Colors&Fonts. This will generate random text color and background color with preserved contrast for readability.

- added: New icon (still temporary).
