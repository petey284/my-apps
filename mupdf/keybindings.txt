A description of each of the supported options is included below.

-p password
        Uses  the  given  password  to  open an encrypted PDF file.  The
        password is tried both as user and owner password.

-r resolution
        Changes the initial zoom level, specified as the  resolution  in
        dpi.  The default value is 72.

MOUSE AND KEY BINDINGS

In  addition to the key bindings described below, the mouse can also be
used. Clicking the left mouse button follows links within the PDF while
dragging  with  the  left mouse button pans the page. Dragging with the
right mouse button selects an area and copies the enclosed text to  the
clipboard  buffer.  Using the scroll-wheel while pressing Control zooms
in/out, if Shift is pressed on the other hand then the page is panned.

L, R   Rotate page left (clockwise) or right (counter-clockwise).

h, j, k, l
        Scroll page left, down, up, or right.

+, -   Zoom in or out.

w      Shrinkwrap window to fit the page.

r      Reload file.

. pgdn right space
        Go to the next page

, pgup left b
        Go to the previous page

<, >   Skip back/forth 10 pages at a time.

m      Mark page for snap back.

t      Pop back to the latest mark.

[0-9]m Save the current page number in the numbered register.

[0-9]t Go to the page saved in the numbered register.

123g   Go to page 123.

/      Search for text.

n, N   Find the next/previous search result.

c      Toggle between color and grayscale rendering.